package ca.dawson.androidmidtermf23

import androidx.lifecycle.ViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow

class MainActivityViewModel : ViewModel() {
    private val _uiState = MutableStateFlow(RollUiState())
    val uiState: StateFlow<RollUiState> = _uiState.asStateFlow()


    init{
        _uiState.value = RollUiState(currentRoll = 1)
    }

    fun rollDice(){
        _uiState.value = RollUiState(currentRoll = (1..6).random())
    }
}