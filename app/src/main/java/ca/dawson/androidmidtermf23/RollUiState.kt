package ca.dawson.androidmidtermf23

data class RollUiState(
    val currentRoll: Int = 1
)
