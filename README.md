# AndroidMidtermF23


This is the starter code for the programming question on our midterm. 

## Study Suggestions

Prior to our midterm, I recommend you do the following

- [ ] Clone this repository and explore the code.
- [ ] Run the code in an AVD to ensure you know what it currently does
- [ ] Create your own private repository for the midterm, using a fork of this code
- [ ] In a separate "practice" branch, try to add a feature of your own devising
